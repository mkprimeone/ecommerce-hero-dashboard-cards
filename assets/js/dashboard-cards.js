(function () {
  'use strict'
  var MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  var config = {
    type: 'line',
    data: {
      labels: MONTHS,
      datasets: [{
        backgroundColor: 'white',
        borderColor: '#ff6384',
        data: [
          200,
          160,
          180,
          210,
          250,
          240,
          230,
          210,
          200,
          180,
          260,
          290
        ],
        fill: false,
      }]
    },
    options: {
      legend: {
        display: false,
      },
      responsive: true,

      plugins: {
        title: {
          display: false,
          text: 'Chart.js Line Chart'
        },
        tooltip: {
          mode: 'index',
          intersect: false,
        }
      },
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        x: {
          display: true,
          title: {
            display: true,
            text: 'Month'
          }
        },
        y: {
          display: true,
          title: {
            display: true,
            text: 'Value'
          }
        },
        yAxes: [{
          ticks: {
            beginAtZero: true,
            suggestedMax: 500,
            stepSize: 100
          }
        }]
      }
    }
  };
  window.onload = function() {
    var ctx = document.getElementById('statistics-canvas').getContext('2d');
    window.myLine = new Chart(ctx, config);
  };

})();
